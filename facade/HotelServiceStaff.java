package com.facade;

public class HotelServiceStaff {
private VegMenu veg;
private NonVeg  nonveg;
public HotelServiceStaff() {
	super();
	this.veg = new VegMenu();
	this.nonveg = new NonVeg();
}
public void showVegMenu() {
	this.veg.showMenu();
}

public void showNonVegMenu() {
	this.nonveg.showMenu();
}
public void showTogather() {
	this.nonveg.showMenu();
	this.veg.showMenu();
}
}
