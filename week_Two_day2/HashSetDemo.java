package com.org.coll2;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

//use Hashing mechanism  , allows null  , not maintain insertion order , best for serach operation 
public class HashSetDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Set<Integer> s1 = new HashSet<Integer>();
s1.add(10);s1.add(22);s1.add(30);s1.add(70);
System.out.println(s1);
HashSet<Integer> s2 = new HashSet<Integer>();
s2.add(22);s2.add(88);s2.add(99);

s1.addAll(s2);
System.out.println(" New Hashset"+s1);

Iterator<Integer> it = s1.iterator();
while(it.hasNext()) {
	System.out.println(it.next());
	System.out.println(" , ");
}
	}

}
