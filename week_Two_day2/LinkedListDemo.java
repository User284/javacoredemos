package com.org.coll2;

import java.util.LinkedList;

/* Declaration of the link list class 
 * public class LinkedList<E>;
extends AbstractSequentiaList<E>;
implements List<E> , Deque<E> , Cloneable */

public class LinkedListDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    LinkedList<String > link = new LinkedList<String>();
    // add some items
    link.add("Java"); link.add("Advance Java");link.add("Freamworks");link.add("SpringBoot");link.add("JDBC");
    for( String s : link)
    	System.out.println(s);
    //add item at specified location 
    link.add(2, "First Course"); link.add(4, " Advance course");
    System.out.println(" Adding at specified location "+ link);
    //adding item at head and tale
    link.addFirst("Kotlin"); link.addLast("JavaScript");
    System.out.println(" Add at first and last "+ link);
    // get and set the item in the list 
    Object  first = link.get(0);
    System.out.println(" at Index 0 :"+ first);
    link.set(0, "Java 9 ");
    System.out.println(" Content after updaing the list "+link);
    
    //remove items 
    link.remove(4);System.out.println(" After removing index 4 value :"+ link);
    link.removeIf(n->(n.charAt(0)=='J'));//charAt is the built in method of String 
    System.out.println(" Removing elements which statrs with 'J' " + link);
    
    
    LinkedList<Integer> it = new LinkedList<Integer>();
    it.add(23);it.add(54);it.add(12);it.add(55);it.add(66);
    System.out.println(" Before removing elements "+ it );
    
   it.removeIf(n->(n%3==0));
    for(int i:it) {
    	System.out.println("after removing items "+i);
    }
    
	}

}
