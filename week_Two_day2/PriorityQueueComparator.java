package com.org.coll2;

import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

public class PriorityQueueComparator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Queue<Book> q1 = new PriorityQueue<Book>();
//creating books
Book b4= new Book(220, "Javascript", "Bpb", "JS", 11);
Book b1 = new Book(101, "Python Book", "BPB", "Galvin", 8);
Book b2 = new Book(107, "DBMS", "Bpb", "Forozon", 7);
Book b3 = new Book(110, "OS", "BPB", "YK", 11);

//adding book in to collection 

q1.add(b1);q1.add(b2);q1.add(b3);q1.add(b4);

Collections.sort(null);

System.out.println("Traverse all the books ");
for(Book b : q1) {
	System.out.println(b.bookId+"  "+b.name+"   "+b.publisher+"   "+b.author+"   "+b.quanity);
}
q1.remove();
System.out.println(" After removing one book from the record ");
for(Book b: q1) {
System.out.println(b.bookId+"  "+b.name+"   "+b.author+"   "+b.publisher+"   "+b.quanity);}
	}

}
