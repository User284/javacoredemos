package com.org.coll2;

import java.util.ArrayList;
import java.util.Collections;

public class StudentDataComparable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
ArrayList<Student> s1 =  new ArrayList<Student>();

s1.add(new Student(101, "Anit", 19));
s1.add(new Student(102, "Shubh", 17));
s1.add(new Student(106, "Rachna", 21));

Collections.sort(s1);
for(Student s : s1) {
	System.out.println(s.rollno +"   "+s.name+"    "+s.age);
}
	}

}
