package com.org.coll2;

import java.lang.reflect.Method;

//use annotation 
class Hello {
	@MyAnnotation(value = 50)
	public void sayHello() {
		System.out.println(" Hello Java Annotation");
	}
}


//Accessing annotation
public class CustomDriverClass {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
Hello hello = new Hello();
Method method = hello.getClass().getMethod("sayHello");

MyAnnotation manno= method.getAnnotation(MyAnnotation.class);
System.out.println(" value is : "+manno.value());
	}

}
