package com.org.coll2;
import java.util.ArrayDeque;
/*To implement a LIFO (Last-In-First-Out) stacks in Java, it is recommended to use a deque over
the Stack class. The ArrayDeque class is likely to be faster than the Stack class.*/
// push , pop , peek 
public class ArrayDequeAsStack {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
ArrayDeque<String > stack = new ArrayDeque<>();

stack.push("Monkey");
stack.push("Cat");
stack.push("Horse");
stack.push("Dog");// last in element 

// access element from top of stack
String element = stack.peek();
System.out.println(" Access Top element : "+ element);

//remove element from top of the stack 

String ele = stack.pop();
System.out.println(" remove the element : "+ele);// first out element

	}

}
