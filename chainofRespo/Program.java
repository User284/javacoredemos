package com.chainofResp;

public class Program {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TeamLeader team = new TeamLeader();
		ProjectManager project = new ProjectManager();
		HR hr = new HR();
		team.setSupervisor(project);
		project.setSupervisor(hr);
		
		team.applyLeave("Sani", 9);
		
		System.out.println();
		
		team.applyLeave("Ashwin", 18);
		
		team.applyLeave("Haritha", 30);
		
		team.applyLeave("AkshaDeep", 50);

	}

}
