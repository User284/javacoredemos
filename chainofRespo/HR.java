package com.chainofResp;

public class HR extends Employee {

	private int max_leave_approve = 30 ;
	@Override
	public void applyLeave(String empname, int days) {
		// TODO Auto-generated method stub
		if(days<= max_leave_approve) {
			approveLeave(empname, days);
		}else {
			System.out.println(" Leave application suspended , please contact HR ");
		}

	}
	private void approveLeave (String empname , int days) {
		System.out.println("HR Manager approved "+ days+ " Leave for the employee" + empname);
	}
}
