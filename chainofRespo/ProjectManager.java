package com.chainofResp;

public class ProjectManager extends Employee {

	private int max_leave_approve= 20 ;
	@Override
	public void applyLeave(String empname, int days) {
		// TODO Auto-generated method stub
    if (days <= max_leave_approve) {
    	approveLeave(empname, days);
    }else {
    	supervisor.applyLeave(empname, days);
    }
	}
	private void approveLeave (String empname , int days) {
		System.out.println("Project Manager approved "+ days+ " Leave for the employee" + empname);
	}
}
