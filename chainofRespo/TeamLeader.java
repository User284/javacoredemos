package com.chainofResp;

public class TeamLeader extends Employee {

	//team lead can only approve upto 10 days of leave 
	
	private int max_leave_approve = 10 ;
	
	@Override
	public void applyLeave(String empname, int days) {
		// TODO Auto-generated method stub
       if(days <= max_leave_approve) {
    	  approveLeave(empname, days);
    	   
       }else {
    	   supervisor.applyLeave(empname, days);
       }
	}
private void approveLeave (String empname , int days) {
	System.out.println("Team Leader approved "+ days+ " Leave for the employee" + empname);
}
}
