package com.chainofResp;

public abstract class Employee {

	//next element in the chain of responsibility 
	
	protected Employee supervisor;
	
	public void setSupervisor(Employee supervisor) {
		this.supervisor=supervisor;
	}
	public abstract void applyLeave( String empname , int days); 
}
