package com.org.multiT2;

public class DemonDemo1 extends Thread {

	public DemonDemo1(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
public void run() {
	if(Thread.currentThread().isDaemon()) {
		System.out.println(getName()+" Is demon thread ");
	}else
	{
		System.out.println(getName()+" Is User thread ");
	}
	System.out.println(getName()+" Priority "+ Thread.currentThread().getPriority());
}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
DemonDemo1 d1= new DemonDemo1("D1");
DemonDemo1 d2= new DemonDemo1("D2");
DemonDemo1 d3= new DemonDemo1("D3");
d1.setDaemon(true);//demon thread 
d1.start();
d2.start();//user thread 
d3.setDaemon(true);//demon thread
d3.start();

	}

}
