package com.org.multiT2;

public class TestThread {
public static Object Lock1= new Object();
public static Object Lock2= new Object();
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		      ThreadDemo1 T1 = new ThreadDemo1();
		      ThreadDemo2 T2 = new ThreadDemo2();
		      T1.start();
		      T2.start();
		   }
	
static	class ThreadDemo1 extends Thread{
		public void run() {
			synchronized (Lock1) {
				System.out.println(" Thread1 : Holding lock1 ....");
				try {
					Thread.sleep(200);
				}catch (Exception e) {
					// TODO: handle exception
				}System.out.println(" Thread 1: waiting for lock 2 ....");
				synchronized (Lock2) {
					System.out.println("Thread 1 : Holding for lock 1 &; lock 2 ");
				}
			}
		}
	}
  static class ThreadDemo2 extends Thread{
	   public void run() {
		   synchronized (Lock2) {
			System.out.println(" Thread 2 : Holding lock 2 ...");
			try {
				Thread.sleep(200);
			}catch (Exception e) {
				// TODO: handle exception
			}System.out.println(" Thread 2 : waiting for lock 1....");
			synchronized (Lock1) {
				System.out.println("Thread 2 : Holding lock 1 & 2....");
			}
		}
	   }
   }
}
