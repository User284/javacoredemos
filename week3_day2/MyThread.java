package com.org.multiT2;

public class MyThread extends Thread {

	public void run() {
		System.out.println(" Thread Running ....."+Thread.currentThread().getName());
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
      MyThread p1 = new MyThread();
      MyThread p2 = new MyThread();
      p1.start();
      p2.start();
      //set priority
      p1.setPriority(3);
      
      p2.setPriority(1);
      //get priority
      int p = p1.getPriority(); int pp = p2.getPriority();
      System.out.println(" First Thread Priority p1 : "+ p +"    Second Thread Priority p2 : "+ pp);
	}

}
