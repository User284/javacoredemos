package com.org.multiT2;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
class Worker extends Thread {
	  BlockingQueue<Integer> q;

	  Worker(BlockingQueue<Integer> q) {
	    this.q = q;
	  }

	  public void run() {
	    try {
	      while (true) {
	        Integer x = q.take();
	        if (x == null) {
	          break;
	        }
	        System.out.println(x);
	      }
	    } catch (InterruptedException e) {
	    }
	  }
	}
public class ConcurrentTest2 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		int capacity = 10;
	    BlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(capacity);

	    int numWorkers = 2;
	    Worker[] workers = new Worker[numWorkers];
	    for (int i = 0; i < workers.length; i++) {
	      workers[i] = new Worker(queue);
	      workers[i].start();
	    }

	    for (int i = 0; i < 10; i++) {
	      queue.put(i);
	    }
	}

}
