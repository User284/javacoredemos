package com.org.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class ArraylIstDemo2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
 ArrayList< String > emp_name = new ArrayList<String>();
 emp_name.add("Amit");emp_name.add("Sumit");emp_name.add("Vineet");emp_name.add("Vijay");
 ArrayList<String> name1 = new ArrayList<String>();
 name1.add("Sumit");name1.add("Payal");name1.add("Vijay");
 //comparing both list 
 boolean value = emp_name.equals(name1);
 System.out.println(" Comparing element of both the list  : "+ value);
 // Uncommon elements 
 ArrayList<Integer> firstlist = new ArrayList<Integer>(Arrays.asList(12,20,21,41,78,47,50,45));
 System.out.println(" first list elemets :"+firstlist);
 ArrayList<Integer> secondlist = new ArrayList<Integer>(Arrays.asList(12,20,21,40,50,45));
 System.out.println("Second List "+ secondlist);
//common elements 
 firstlist.retainAll(secondlist);
 System.out.println("Common elements in both the list : ");
 System.out.println(firstlist);
 
 //Uncommon elements 
 firstlist.removeAll(secondlist);
 System.out.println("Uncommon elements of the list : "+firstlist);
 
 
 //traverse list through iterator
 Iterator<String> itr = emp_name.iterator();
 while(itr.hasNext()) {
	System.out.println(itr.next()); 
 }
 
 //traverse by for each loop 
 
 for(String name : emp_name)
	 System.out.println("Employee Name : " +name);
	}

}
