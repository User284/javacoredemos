package com.org.generic;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
//program to perform different operations based on methods defined by the SortedSet interface.
public class TreeSetEx3 {
	public static void main(String[] args) 
	{
	 Set<String> s = new HashSet<String>();
	  s.add("Delhi");
	  s.add("Boston");
	  s.add("New York");
	  s.add("Paris");
	  s.add("London");
	  s.add("Delhi"); // Adding duplicate elements.
	  
	TreeSet<String> ts = new TreeSet<>(s);
	System.out.println("Sorted TreeSet: " +ts);

	// Using methods of SortedSet interface.
	  System.out.println("First Element: " +ts.first());
	  System.out.println("Last Element: " +ts.last());
	  System.out.println("HeadSet Elements line 24 : " +ts.headSet("London"));// London not include 
	  System.out.println("TailSet Elements  line 25: " +ts.tailSet("London"));// london will include 
	  
	SortedSet<String> subSet = ts.subSet("Delhi", "Paris");
	System.out.println("SubSet Elements: " +subSet);
	System.out.println("Sorted Set: " +ts.comparator()); // It will return null because natural ordering is used.
	  }
}
