package com.org.generic;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
//HashMap is the best choice if our frequent operation is a search operation.
// map interface -HashMap , LinkedHashMap , TreeMap
public class HashMapDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method st
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		map.put("ab123", 3000);
		map.put("ab122", 7000);
		map.put("ab456", 8000);
		map.put("ab122", null);
		map.put("ab123", 8000);//duplicate key
		 map.put(null, 6000);
		map.put(null, null);//null key and null value is allowed 
	   
	    
	    System.out.println("-----MAP Data ---------"+map);
	    
	    for(Map.Entry m : map.entrySet()) {
	    	System.out.println(m.getKey()+ "    "+m.getValue());
	    }
	    map.putIfAbsent("am555", 11000);//aa123, 11000 ,am123", 11000
	    System.out.println(" After inserting putifabsent() ");
	    for(Map.Entry m : map.entrySet()) {
	    	System.out.println(m.getKey()+ "    "+m.getValue());
	    }
	    HashMap<String, Integer> map1 = new HashMap<String, Integer>();
	    map1.put("xy123", 34000);map1.put("xy345", 45000);map1.put("ab123", 15000);
	    System.out.println(" After invoking putall() ");
	    map.putAll(map1);
	    for(Map.Entry m : map.entrySet()) {
	    	System.out.println(m.getKey()+ "    "+m.getValue());
	    }
	    
	}

}
