package com.org.generic;

import java.util.Set;
import java.util.TreeSet;

//treeset class implements set interface uses tree for storage and extends AbstractSet class 
//treeset object stored in ascending order 
//access time is fast , it is not allow null values 
//elements are kept in sorted, ascending order in the tree set.
//TreeSet is an excellent choice for quick and fast access to large amounts of sorted data.
public class TreeSetDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Set<String> ts = new TreeSet<String>();
//check set is empty or not 
boolean empty = ts.isEmpty();
System.out.println(" Treeset is Empty ? "+ empty);

//check the size 

int i = ts.size();
System.out.println(" Size of Treeset = "+ i);

//adding the element
ts.add(" India"); ts.add("USA"); ts.add("Australia");ts.add("New Zealand"); ts.add("Switzerland");

System.out.println("Sorted Treeset : "+ ts);
int size = ts.size();
System.out.println(" Size of Treeset Now : "+ size);

	}

}
