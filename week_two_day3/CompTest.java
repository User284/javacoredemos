package com.org.generic;

import java.util.Comparator;
import java.util.TreeSet;

class RevStrComp implements Comparator<String>{

	@Override
	public int compare(String o1, String o2) {
		// TODO Auto-generated method stub
		return o2.compareTo(o1);
	}
	
}
public class CompTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
RevStrComp rsc = new RevStrComp();

TreeSet<String> ts = new TreeSet<String>(rsc);
ts.add("Cat");ts.add("Lion");ts.add("Tiger");ts.add("Horse");ts.add("Elephant");
System.out.println(" Sorted in reverse order ");
for(String e : ts) {
	System.out.print(e+"  ");
	System.out.println();
}
	}

}
