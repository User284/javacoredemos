package com.factory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GenerateBill {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
     UsePlan pp = new UsePlan();
     System.out.println(" Please select the plan to generate the bill ");
     BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
     
     String name = bf.readLine();
     System.out.println(" mention the number of units ");
     int units = Integer.parseInt(bf.readLine());
     
     Plan p = pp.getPlan(name);
     System.out.println(" Generate Bill amount  "+name +"units "+units );
     p.getRate();
     p.calculateBill(units);
	}

}
