package com.factory;

public class UsePlan {

	public Plan getPlan(String planType) {
		if(planType == null) {
			return null ;
		}
		if( planType.equalsIgnoreCase("generalplan")) {
			return new GeneralPlan();
		}
		else if (planType.equalsIgnoreCase("businessplan")) {
			return new BusinessPlan();
		}
		else if(planType.equalsIgnoreCase("Corporateplan")) {
			return new CorporatePlan();
		}
		return null;
	}
}
