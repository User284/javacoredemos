package com.org.jdk8;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

//machine read  data from source 
//inputstream classes - fileinputstream, filterinputstream, objectinputstream 
//FileInputStram extends InputStream

public class InputStreamDemo {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
			 try {
			
			        FileInputStream f1 = new FileInputStream("D://p1.txt");

			        System.out.println("Data in the file: ");

			        // Reads the first byte
			        int i = f1.read();

while(i!=-1) {
System.out.println((char)i);
//then read next byte from the file
i= f1.read();
}f1.close();
		}
		catch (FileNotFoundException e) {
			// TODO: handle exception
			System.out.println(e);
		}
	
	
	}
}

