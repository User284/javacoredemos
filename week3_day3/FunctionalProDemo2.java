package com.org.jdk8;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FunctionalProDemo2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
 List<Integer> num = Arrays.asList(11,22,55,66,77,88,110);
 
 //java 7 
 for(int i=0 ;i<num.size();i++) {
	 System.out.println(num.get(i)+"   ");
 }
 
 //java 8 
 num.forEach(number
         -> System.out.print(
             number + " "));
 
	}

}
