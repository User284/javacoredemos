package com.org.day3;

public class MatrixAdditionExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
  int a[][]= {{1,2,3},{2,4,3},{3,4,5}};
  int b[][] = {{1,3,4},{2,4,3},{1,2,4}};
  
  //creating another matrix to store the result
  int result[][] = new int[3][3];
  
  for(int i = 0 ; i<3;i++) {
	  for(int j =0 ;j<3;j++) {
		  result[i][j]= a[i][j]+b[i][j];// for addition of two matrix
		  System.out.print(result[i][j] +"    ");
	  }
	  System.out.println();
  }
  
	}

}
