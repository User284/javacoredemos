package com.org.jdk8_2;

import java.sql.Array;
import java.util.ArrayList;
import java.util.stream.Collectors;

/*Stream is sequence of Objects , and it is used for the process the collections of objects
 * it is the sequence of the object which can be pipelined to produce the desired result 
 * 
 * if we want to represent a group of objects as single entity then we should go for collection 
 * if we want to process objects from the collection then we should go for the stream.*/
public class StreamDemo1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// without using stream filter 
ArrayList<DigitalProduct> pro = new ArrayList<DigitalProduct>();
pro.add(new DigitalProduct(123, "Laptop", 60000));
pro.add(new DigitalProduct(111, "Mobile phone", 80000));
pro.add(new DigitalProduct(222, "Smart Watch", 90000));
pro.add(new DigitalProduct(134, "Alexa", 50000));
pro.add(new DigitalProduct(456, "Moniter", 50000));
pro.add(new DigitalProduct(435, "Ipad", 50000));
System.out.println(pro);
	
	ArrayList<Integer> price = new ArrayList<Integer>();
	 for(DigitalProduct dp : pro) {
		 if(dp.proPrice <80000) {
			price.add(dp.proPrice);
		 }
	 }
	 System.out.println(price);
//using stream 
	 ArrayList<Integer> pp = (ArrayList<Integer>) pro.stream().filter(p->p.proPrice<80000)//filtering data
			                                  .map(p->p.proPrice)//fetching price
			                                  .collect(Collectors.toList());
	 System.out.println(pp);
	 
	 System.out.println(" List product name whose price is 50000");
	 pro.stream().filter(digipro ->digipro.proPrice==50000).forEach(digipro->System.out.println(digipro.proName+"  "+digipro.proPrice));

	//using collector to sum all the product price 
	int totalPrice = pro.stream().collect(Collectors.summingInt(d->d.proPrice));
	System.out.println(" Total price = "+ totalPrice);
	
	}
}