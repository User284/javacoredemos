package com.org.exception;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class MainComparator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
ArrayList<Student> s1 = new ArrayList<Student>();
s1.add(new Student("Radhika", 17, 80));
s1.add(new Student(" Amit", 16, 89));
s1.add(new Student("Babu", 19, 78));
s1.add(new Student("Rohan", 15, 90));
System.out.println(" Sort by age");

Collections.sort(s1,new AgeComparator());
Iterator it = s1.iterator();
while(it.hasNext()) {
Student st = (Student) it.next();
System.out.println(" Name "+st.name+ " Age "+ st.age+" marks "+ st.marks);
}

System.out.println("\n");
System.out.println(" Sort by Marks");
Collections.sort(s1,new MarkComparator());
Iterator itr = s1.iterator();
while(itr.hasNext()) {
Student st = (Student) itr.next();
System.out.println(" Name "+st.name+ " Age "+ st.age+" marks "+ st.marks);
}
	}

}
