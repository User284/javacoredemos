package com.org.exception;

public class GenericMethodTest {

	public static <T> void printGenericArray(T[] items) {
		for(T item : items ) {
			System.out.print(item+"  ");
		}
		System.out.println();
	}
	
	static <T> void genericDisp(T element) {
		System.out.println(element.getClass().getName()+"  =   "+element);
		
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
      Integer[] int_array = {1,3,5,7,9,11};
      Character[] char_array = {'J','A','V','A', 'D','E','M','O'};
      
      System.out.println("Integer Array ---");
      printGenericArray(int_array);
      
      System.out.println("Character Array ---");
      printGenericArray(char_array);
      
      //call method gericDisp 
      
      genericDisp(1223);
      
      genericDisp("java Generic Demo");
      
      genericDisp(1.0);
      
	}

}
