package com.org.exception;

// Final , Finally , finalize

public class TestClass {
	static //use of the final key word 
	int money =5000 ;
	 final int age = 18 ; // final is like constant declaration 
	 void disp() {
		// age=55; // reassigning value to age constant variable 
	 }
	 
public static void main(String args[]) {
	//use of finally
	try {
		money=money/0;
		System.out.println(money);
	}catch (Exception e) {
		// TODO: handle exception
		System.out.println(e);
	}
	finally {
		System.out.println(" I am finally block");
	}
	
	TestClass tt= new TestClass();
	System.out.println(" object :"+ tt);
	tt.disp();
	//use of finalize 
//	System.out.println(" Hashcode for class object :"+ tt.hashCode());
	tt=null;
	System.out.println("present object condition :"+tt);
	System.gc();
	System.out.println(" End of the garbage collection");	
}
    protected void finalize() {
    	System.out.println(" Called the finalize method");
    }
}
