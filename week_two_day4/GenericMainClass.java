package com.org.exception;

//Generic class
class Test_Generic<T1,T2>
{
	T1 obj1; 
	T2 obj2;
	public Test_Generic(T1 obj1, T2 obj2) {
		super();
		this.obj1 = obj1;
		this.obj2 = obj2;
	}
	public void print()
	{
		System.out.println("T1 Object : "+ obj1);
		System.out.println("T2 Object : "+ obj2);
	}
}

//driver class using generic type 
public class GenericMainClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Test_Generic<String , Integer> test = new Test_Generic<String, Integer>("Java Generic" , 11);
		Test_Generic<Integer , Integer> t1 = new Test_Generic(12, 20);
		t1.print();
		test.print();

	}

}
