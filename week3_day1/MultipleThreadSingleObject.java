package com.org.multiT1;

public class MultipleThreadSingleObject implements Runnable {
String task;

	public MultipleThreadSingleObject(String task) {
	super();
	this.task = task;
}

	@Override
	public void run() {
		// TODO Auto-generated method stub
for(int i =1 ; i<=5 ; i++) {
	System.out.println(task+" : "+ i);
}try {
	Thread.sleep(1000);
}catch (Exception e) {
	// TODO: handle exception
	e.printStackTrace();
}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Thread th = Thread.currentThread();
		System.out.println("Name of the current Thread : "+ th);
		
     MultipleThreadSingleObject obj = new MultipleThreadSingleObject("Multiple thread with single object ");
     Thread t1= new Thread(obj);t1.start();
     Thread t2= new Thread(obj);t2.start();
     Thread t3= new Thread(obj);t3.start();
     
     int count = Thread.activeCount();
     System.out.println(" No of active threads : "+ count);
     
	}

}
