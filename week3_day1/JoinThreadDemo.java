package com.org.multiT1;
// join method used to wait the current thread until the thread has called the join method complete execution
public class JoinThreadDemo extends Thread {

	public void run() {
		System.out.println(" child thread is running ..");
		for( int i=0 ; i<=4; i++) {
			System.out.println(i);
		}
		System.out.println(" Child thread is ending");
	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
       JoinThreadDemo j1 = new JoinThreadDemo();
    		   j1.start();// thread j1 is ready to run 
    		   try {
    			   j1.join();
    		   }catch (Exception e) {
				// TODO: handle exception
    			   System.out.println(e);
			}
    		   System.out.println(" Main thread is ending...");
	}

}
