package com.org.multiT1;

public class SleepThreadDemo extends Thread {

	public void run() {//running state
		
		for(int i =1 ;i<5 ; i++) {
			try {
				//Thread.sleep(0);
				Thread.sleep(800, 50);
			}catch (Exception e) {
				System.out.println(e);
			}
			System.out.println(i);
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
      SleepThreadDemo s1 = new SleepThreadDemo();//  1 thread object new state
      SleepThreadDemo s2 = new SleepThreadDemo();//2 thread object New state 
      
      // put thread into runnable state 
      s1.start();
      s2.start();
	}

}
