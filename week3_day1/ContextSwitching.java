package com.org.multiT1;

public class ContextSwitching implements Runnable {

	@Override
	public void run() {
		// TODO Auto-generated method stub
		for(int i = 1 ; i<=4 ;i++) {
			try {
				Thread.sleep(1000);
			}catch (InterruptedException  e) {
				// TODO: handle exception
				System.out.println(e);
			}System.out.println(Thread.currentThread()+" i : "+ i );
		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
 ContextSwitching a1 = new ContextSwitching(); // task object 
 
 Thread t1 = new Thread(a1, "First Child Thread");//child thread
 
 ContextSwitching a2 = new ContextSwitching();//task object 
 
 Thread t2 = new Thread(a2, " Second Child Thread");//child thread
 
 t1.start();//main thread start execution when t1 .start() method is called
 t2.start();
	}

}
