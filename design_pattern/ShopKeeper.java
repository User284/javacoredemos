package com.builder;

public class ShopKeeper {
public static Juice takeOrder(String JuiceType) {
	JuiceMaker jm = null;
	if(JuiceType.equalsIgnoreCase("Orange")) {
		jm= new OrangeJuice();
	}
	if(JuiceType.equalsIgnoreCase("Apple")) {
		jm= new AppleJuice();
	}
	
	else {
        System.out.println("Sorry we don't take order for  " + JuiceType);
    }
	return jm.makeJuice();

}
}
