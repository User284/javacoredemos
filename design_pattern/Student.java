package com.builder;

public class Student {
private String fname;//imp
private String lname;//imp
private String age;
private String gender ;
private String address;
private boolean isearing; 
private boolean hasExperience;

private Student(StudentBuilder builder) {
	// TODO Auto-generated constructor stub
	this.fname = builder.fname;
	this.lname = builder.lname;
	this.age = builder.age;
	this.gender = builder.gender;
	this.address =builder. address;
	this.isearing = builder.isearing;
	this.hasExperience = builder.hasExperience;
}
@Override
public String toString() {
	return "Student [fname=" + fname + ", lname=" + lname + ", age=" + age + ", gender=" + gender + ", address="
			+ address + ", isearing=" + isearing + ", hasExperience=" + hasExperience + "]";
}
public  static class StudentBuilder {

	private String fname;
	private String lname;
	private String age;
	private String gender ;
	private String address;
	private boolean isearing; 
	private boolean hasExperience;
	public StudentBuilder(String fname, String lname) {
		super();
		this.fname = fname;
		this.lname = lname;
	}
	public StudentBuilder addAge(String age) {
	
		this.age = age;
		return this;
	}
	public StudentBuilder addGender(String gender) {
		//super();
		this.gender = gender;
		return this;
	}
	public StudentBuilder addaddress(String address) {
        this.address = address;
        return this;
    }
    public StudentBuilder addIsEarning(boolean isearing) {
        this.isearing= isearing;
        return this;
    }
    public StudentBuilder addHasExperience(boolean hasExperience) {
        this.hasExperience = hasExperience;
        return this;
    }
    public Student build() {
    	return new Student (this);
    }

	
	
	
}

}
