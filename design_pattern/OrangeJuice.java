package com.builder;

public class OrangeJuice extends JuiceMaker {

	@Override
	void setJuiceType() {
		// TODO Auto-generated method stub
      System.out.println("Orange");
      getJuice().setJuicename("Orange");
	}

	@Override
	void setWater() {
		// TODO Auto-generated method stub
         System.out.println("Step 1 : Adding water into the juice");
         getJuice().setWater(40);
	}

	@Override
	void setFruit() {
		// TODO Auto-generated method stub
          System.out.println("step 2 : Adding fruit into the Juicer");
          getJuice().setFruit(4);
	}

	@Override
	void setSugar() {
		// TODO Auto-generated method stub
      System.out.println("Step 3 : Addign suger into the Juice");
      getJuice().setSugar(6);
	}

	@Override
	Juice createJuice() {
		// TODO Auto-generated method stub
		return new Juice();
	}

}
