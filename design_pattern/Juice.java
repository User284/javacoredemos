package com.builder;

public class Juice {
private int water ; 
private int fruit; 
private int sugar; 
private String juicename;
public int getWater() {
	return water;
}
public void setWater(int water) {
	this.water = water;
}
public int getFruit() {
	return fruit;
}
public void setFruit(int fruit) {
	this.fruit = fruit;
}
public int getSugar() {
	return sugar;
}
public void setSugar(int sugar) {
	this.sugar = sugar;
}
public String getJuicename() {
	return juicename;
}
public void setJuicename(String juicename) {
	this.juicename = juicename;
}
@Override
public String toString() {
	return "Juice [water=" + water + ", fruit=" + fruit + ", sugar=" + sugar + ", juicename=" + juicename + "]";
}
}
