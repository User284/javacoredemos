package com.decorator;

public abstract class Beverage {
  String description = "Beverage";



public String getDescription() {
	return description;
}
// this methods abstract because different beverage have different cost 
public abstract double cost();


}
