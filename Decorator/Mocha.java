package com.decorator;

public class Mocha extends CondimentDecorator {
private Beverage beverage;


	public Mocha(Beverage beverage) {
	super();
	this.beverage = beverage;
}


	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return beverage.getDescription()+" , Mocha";
	}
	public double cost() {
		return beverage.cost()+1.5d;
	}

}
