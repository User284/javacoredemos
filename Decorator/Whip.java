package com.decorator;

public class Whip extends CondimentDecorator {
private Beverage beverage;

	public Whip(Beverage beverage) {
	super();
	this.beverage = beverage;
}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return beverage.getDescription()+", whip";
	}
	public double cost() {
		return beverage.cost()+2.5d;
	}

}
