package com.decorator;

public class RedShapeDecorator extends ShapeDecorator {

	public RedShapeDecorator(Shap decoratedShape) {
		super(decoratedShape);
		// TODO Auto-generated constructor stub
	}
public void draw() {
	decoratedShape.draw();
	setRedBorder(decoratedShape);
}

private void setRedBorder(Shap decoratedShape) {
	System.out.println(" Border color : Red ");
}
}
