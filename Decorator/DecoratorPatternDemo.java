package com.decorator;

public class DecoratorPatternDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
Shap circle = new Circle();

Shap redCircle = new RedShapeDecorator(new Circle());

Shap redRectangle = new RedShapeDecorator(new Rectangle());


System.out.println(" Circle with normal border ");

circle.draw();

System.out.println(" Circle with red border");

redCircle.draw();

System.out.println(" Rectangle with red border ");
redRectangle.draw();
	}

}
