package com.org.jdk81;
//a Lambda expression (or function) is an anonymous function,a function with no name and any identifier.
//nameless functions given as constant values and written exactly in the place where it's needed 
/* syntax ==    (parameter)-> expression ---> (x,y)->x+y */



interface Calculate{
	public void cal();
}


public class LambdaDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
  int num = 10;
  
  //without lambda expression 
  
  Calculate c= new Calculate() {
	
	@Override
	public void cal() {
		// TODO Auto-generated method stub
		System.out.println((2+2) +"   result ");
	}
};c.cal();
  
// with lambda expression 

Calculate c1=()->System.out.println("result = "+(2*4));
c1.cal();


	}

}
